package com.taradevko.aem;

public class Coffee {

	private CoffeeBean bean;

	public Coffee(final CoffeeBean bean) {
		this.bean = bean;
	}

	@Override
	public String toString() {
		return "I am full of: " + bean;
	}
}
