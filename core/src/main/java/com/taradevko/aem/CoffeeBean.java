package com.taradevko.aem;

public class CoffeeBean {

	private String content;

	public CoffeeBean(final String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return content;
	}
}
