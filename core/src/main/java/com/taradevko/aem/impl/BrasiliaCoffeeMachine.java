package com.taradevko.aem.impl;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.taradevko.aem.BeansProvider;
import com.taradevko.aem.Coffee;
import com.taradevko.aem.CoffeeMachine;

@Component(service = CoffeeMachine.class)
public class BrasiliaCoffeeMachine implements CoffeeMachine {

	@Reference(target = "(type=bitter)")
	BeansProvider bitterBeansProvider;

	@Reference(target = "(type=softer)")
	BeansProvider softerBeansProvider;

	@Override
	public Coffee getBitterCoffee() {
		return new Coffee(bitterBeansProvider.getBean());
	}

	@Override
	public Coffee getSofterCoffee() {
		return new Coffee(softerBeansProvider.getBean());
	}
}
