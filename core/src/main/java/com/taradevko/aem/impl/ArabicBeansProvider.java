package com.taradevko.aem.impl;

import org.osgi.service.component.annotations.Component;

import com.taradevko.aem.CoffeeBean;
import com.taradevko.aem.BeansProvider;

@Component(service = BeansProvider.class, property = "type=softer")
public class ArabicBeansProvider implements BeansProvider {

	@Override
	public CoffeeBean getBean() {
		return new CoffeeBean("I am arabica");
	}
}
