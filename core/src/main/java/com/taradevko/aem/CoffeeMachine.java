package com.taradevko.aem;

public interface CoffeeMachine {

	Coffee getBitterCoffee();

	Coffee getSofterCoffee();
}
