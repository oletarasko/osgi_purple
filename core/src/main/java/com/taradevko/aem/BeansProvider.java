package com.taradevko.aem;

public interface BeansProvider {

	CoffeeBean getBean();
}
