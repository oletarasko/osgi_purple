package com.taradevko.aem;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
	service = Servlet.class,
	property = {
			"sling.servlet.extensions=json",
			"sling.servlet.paths=/bin/foo",
			"sling.servlet.methods=get"
	}
)
public class CoffeeConsumerServlet extends SlingSafeMethodsServlet {

	@Reference
	private CoffeeMachine coffeeMachine;

	@Override
	protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws IOException {
		PrintWriter writer = response.getWriter();
		writer.write(String.format("{ bitter: '%s', soft: '%s'}", coffeeMachine.getBitterCoffee(), coffeeMachine.getSofterCoffee()));
	}
}
